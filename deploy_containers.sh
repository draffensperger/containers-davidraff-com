awk '
BEGIN {                # The BEGIN block is executed before the files are read
    FS=":|: "          # Set the file separator to be either a colon or a colon space
    OFS=":"            # Set the OFS (output field separator) to be a color
}
FNR==NR {              # FNR==NR only true when reading the first file
   if ($1 != "") {
     key2value["secrets." $1]=$2;   # Create associative array of ("secrets." + key),value pairs 
   }
   next                # Grab the next line in the first file
} 
{                      # Now in the second file, print looked up value and replace all of our secrets.key,value pairs
  for (key in key2value) {
     #print key ":" key2value[key]
     sub(key, key2value[key])       
  }
  print;
}
' secrets.yaml containers.template.yaml > containers.yaml

echo containers.yaml:
echo =====================
cat containers.yaml
echo =====================
echo Uploading containers.yaml to Google compute cloud ...
gcloud compute instances add-metadata instance1 --zone us-central1-f --metadata-from-file google-container-manifest=containers.yaml
rm containers.yaml
echo Restart kubelet service via SSH ...
ssh reimburse.davidraff.com 'sudo service kubelet restart'

